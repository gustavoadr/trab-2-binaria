#include <stdio.h>
#include <stdlib.h>
#include "ArvoreBinaria.h"

#define tam 25					//tamanho vetor de codigos
#define intervalo 300			//intevalo aleatorio dos codigos

int main(){
    int dados[tam];
   
    for(int i=0; i < tam; i++)
    	dados[i] = rand() % intervalo;

    ArvBin* raiz = cria_ArvBin();

    int i;
    for(i=0; i < N; i++)
        insere_ArvBin(raiz,dados[i]);

    libera_ArvBin(raiz);

    return 0;
}
